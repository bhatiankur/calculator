package calculator.output;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.operations.Operator;
import calculator.operations.SimpleOperatorImpl;

/**
 * These tests are used to verify the output constraints of the result being displayed.
 * 
 * @author Ankur Bhatia
 *
 */
public class OutputTests {

	Operator oper = new SimpleOperatorImpl();
	String args [] = new String [] {"1333333.0","+","333333333.0"};

	@Test
	public void check_That_Thousand_Separator_Is_Not_Present_In_Result(){
		
		System.out.println("Executing......check_That_Thousand_Separator_Is_Not_Present_In_Result()");
		System.out.println("-----------------------------------------------------------------------------------");
				
		String result = oper.perform(args[0], args[2], args[1]).toString();
		assertFalse(result.contains(","));
		
		System.out.println("###################################################################################");
	}
	
}
