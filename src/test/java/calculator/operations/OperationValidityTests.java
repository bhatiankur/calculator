package calculator.operations;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.operations.Operator;
import calculator.operations.SimpleOperatorImpl;

/**
 * These tests are used to verify that the calculator performs basic arithmetic operations correctly.
 * 
 * @author Ankur Bhatia
 *
 */
public class OperationValidityTests {
	
	Operator calc = new SimpleOperatorImpl();
	String argsAdd [] = new String [] {"1.0","+","2.0"};
	String argsSubtract [] = new String [] {"2","-","1"};
	String argsMultiply [] = new String [] {"4.0","x","2.0"};
	String argsDivision [] = new String [] {"3","/","2"};
	
	@Test
	public void check_that_the_calculator_adds_correctly(){
		
		System.out.println("Executing......check_that_the_calculator_adds_correctly()");
		System.out.println("-----------------------------------------------------------------------------------");
			
		assertNotNull(calc.perform(argsAdd[0], argsAdd[2], argsAdd[1]));
		assertEquals(3, calc.perform(argsAdd[0], argsAdd[2], argsAdd[1]).intValue());
		
		System.out.println("###################################################################################");
	}
	
	@Test
	public void check_that_the_calculator_subtracts_correctly(){
		
		System.out.println("Executing......check_that_the_calculator_subtracts_correctly()");
		System.out.println("-----------------------------------------------------------------------------------");
		
		
		assertNotNull(calc.perform(argsSubtract[0], argsSubtract[2], argsSubtract[1]));
		assertEquals(1, calc.perform(argsSubtract[0], argsSubtract[2], argsSubtract[1]).intValue());
		
		System.out.println("###################################################################################");
	}
	
	
	@Test
	public void check_that_the_calculator_divides_correctly(){
		
		System.out.println("Executing......check_that_the_calculator_divides_correctly()");
		System.out.println("-----------------------------------------------------------------------------------");
		
		assertNotNull(calc.perform(argsDivision[0], argsDivision[2], argsDivision[1]));
		assertEquals("1.50", calc.perform(argsDivision[0], argsDivision[2], argsDivision[1]).toString());
		
		System.out.println("###################################################################################");
		
	}
	
	@Test
	public void check_that_the_calculator_multiplies_correctly(){
	
		System.out.println("Executing......check_that_the_calculator_multiplies_correctly()");
		System.out.println("-----------------------------------------------------------------------------------");
		
		assertNotNull(calc.perform(argsMultiply[0], argsMultiply[2], argsMultiply[1]));
		assertEquals(8, calc.perform(argsMultiply[0], argsMultiply[2], argsMultiply[1]).intValue());
		
		System.out.println("###################################################################################");
	}
	

}
