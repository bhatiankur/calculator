package calculator.input;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import calculator.filtering.ArgumentCountCheckerImpl;
import calculator.filtering.DivideByZeroCheckerImpl;
import calculator.filtering.NegativeNumberCheckerImpl;
import calculator.filtering.NumberPresentCheckerImpl;
import calculator.filtering.OperationSupportedCheckerImpl;
import calculator.filtering.PreprocessingFilter;

/**
 * These tests are used to verify the input arguments being sent to the calculator
 * 
 * @author Ankur Bhatia
 *
 */
public class InputTests {
	
	
	String argsGood [] = new String [] {"1","/","2.000005"};
	String argsBad [] = new String [] {"-1","%","454545454554y","5"};
	String argsEmpty [] = new String [] {};
	String argsDivideByZero [] = new String [] {"1","/","0"}; 
	
	@Test
	public void notify_user_if_required_input_arguments_are_not_provided(){
		
		System.out.println("Executing......notify_user_if_required_input_arguments_are_not_provided()");
		System.out.println("-----------------------------------------------------------------------------------");
	
		
		PreprocessingFilter checker = new ArgumentCountCheckerImpl();
		assertFalse(checker.evaluate(argsEmpty));
		
		
		System.out.println("###################################################################################");
	}
	
	@Test
	public void notify_user_if_extra_input_arguments_are_provided(){
		
		System.out.println("Executing......notify_user_if_extra_input_arguments_are_provided()");
		System.out.println("-----------------------------------------------------------------------------------");
		
		PreprocessingFilter checker = new ArgumentCountCheckerImpl();
		assertFalse(checker.evaluate(argsBad)); 
		assertTrue(checker.evaluate(argsGood));
		
		
		System.out.println("###################################################################################");
	}
	
	@Test
	public void notify_user_if_input_operands_are_invalid_due_to_being_negative(){
		
		System.out.println("Executing......notify_user_if_input_operands_are_invalid_due_to_being_negative()");
		System.out.println("-----------------------------------------------------------------------------------");
		
		PreprocessingFilter checker = new NegativeNumberCheckerImpl();
		
		assertFalse(checker.evaluate(argsBad));
		assertTrue(checker.evaluate(argsGood));
		
		System.out.println("###################################################################################");
	}
	
	@Test
	public void notify_user_if_input_operands_are_invalid_due_to_not_being_a_number(){
		
		System.out.println("Executing......notify_user_if_input_operands_are_invalid_due_to_not_being_a_number()");
		System.out.println("-----------------------------------------------------------------------------------");
		
		PreprocessingFilter checker = new NumberPresentCheckerImpl();
		assertFalse(checker.evaluate(argsBad));
		assertTrue(checker.evaluate(argsGood));
		
		System.out.println("###################################################################################");
	}
	
	
	@Test
	public void notify_user_if_operation_is_not_supported(){
		
		System.out.println("Executing......notify_user_if_operation_is_not_supported()");
		System.out.println("-----------------------------------------------------------------------------------");
		
		PreprocessingFilter checker = new OperationSupportedCheckerImpl();
		assertFalse(checker.evaluate(argsBad));
		assertTrue(checker.evaluate(argsGood));
		
		
		System.out.println("###################################################################################");
	}
	
	@Test
	public void notify_user_that_division_by_zero_is_not_allowed(){
		System.out.println("Executing......notify_user_that_division_by_zero_is_not_allowed()");
		System.out.println("-----------------------------------------------------------------------------------");
		
		PreprocessingFilter checker = new DivideByZeroCheckerImpl();
		
		assertFalse(checker.evaluate(argsDivideByZero));
		assertTrue(checker.evaluate(argsGood));
		
		System.out.println("###################################################################################");
	}

}
