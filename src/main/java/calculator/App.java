package calculator;

import java.util.ArrayList;

import calculator.filtering.ArgumentCountCheckerImpl;
import calculator.filtering.DivideByZeroCheckerImpl;
import calculator.filtering.NegativeNumberCheckerImpl;
import calculator.filtering.NumberPresentCheckerImpl;
import calculator.filtering.OperationSupportedCheckerImpl;
import calculator.filtering.PreprocessingFilter;
import calculator.operations.Operator;
import calculator.operations.SimpleOperatorImpl;
import calculator.utilities.Constants;

/**
 * The root application which is called by the end user. This uses the following entities - 
 * 	1) A set of pre-processing filters which are used to validate the operands and the operation requested.
 *  2) The operator which actually performs the desired operation and returns the result to the end user.
 *  
 * @author Ankur Bhatia
 *
 */
public class App {
	
	private Operator operator = null;
	private ArrayList<PreprocessingFilter> filterList = null;
	
	public App(){
		
	}
		

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		App calcApp = new App();
		calcApp.initialize();
		calcApp.process(args);
	}


	private void process(String[] args) {
		
		for(PreprocessingFilter check : this.getFilterList()){
			if(!check.evaluate(args)){
				System.out.println(Constants.baseErrorMessage);
				return;				
			}
		}
		
		System.out.println(Constants.baseSuccessMessage + this.operator.perform(args[0], args[2], args[1]));
	}


	private void initialize() {
		
		this.setOperation(new SimpleOperatorImpl());
		
		ArrayList<PreprocessingFilter> filterList = new ArrayList<PreprocessingFilter>();
		filterList.add(new OperationSupportedCheckerImpl());
		filterList.add(new ArgumentCountCheckerImpl());
		filterList.add(new NegativeNumberCheckerImpl());
		filterList.add(new DivideByZeroCheckerImpl());
		filterList.add(new NumberPresentCheckerImpl());
		
		this.setFilterList(filterList);
	}

	public Operator getOperation() {
		return operator;
	}


	public void setOperation(Operator operation) {
		this.operator = operation;
	}


	public ArrayList<PreprocessingFilter> getFilterList() {
		return filterList;
	}


	public void setFilterList(ArrayList<PreprocessingFilter> filterList) {
		this.filterList = filterList;
	}


}
