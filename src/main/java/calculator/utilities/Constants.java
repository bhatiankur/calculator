package calculator.utilities;

import java.util.HashMap;

/**
 * This class provides various utilities to the calculator app.
 * 	- General error messages
 *  - String constants
 *  - Utility Collections
 *     
 * @author Ankur Bhatia
 *
 */
public class Constants {

	public static final String addSymbol = new String("+");
	public static final String subtractSymbol = new String("-");
	public static final String multiplySymbol = new String("x");
	public static final String divideSymbol = new String("/");
	
	public static final String addAction = new String("add");
	public static final String subtractAction = new String("subtract");
	public static final String multiplyAction = new String("multiply");
	public static final String divideAction = new String("divide");
	
	
	public static String desiredOperandExpression = new String ("[0-9]+[.]?[0-9]*");
	
	public static String supportedOperations [] = { addSymbol, subtractSymbol, multiplySymbol, divideSymbol	};
		
	public static HashMap<String, String> supportedOperationsMap = new HashMap<String, String>();
	
	static {
		supportedOperationsMap.put(addSymbol, addAction);
		supportedOperationsMap.put(subtractSymbol, subtractAction);
		supportedOperationsMap.put(multiplySymbol, multiplyAction);
		supportedOperationsMap.put(divideSymbol, divideAction);
	}
	
	public static String countErrorMessage = "ERROR - The calculator accepts 3 inputs in this format [operand] [operator] [operand]";
	public static String supportedOperationsErrorMessage = "ERROR - The calculator currently supports the following operations ";
	public static String negativeNumberNotSupportedErrorMessage = "ERROR - The calculator only supports positive (+ve) operands.";
	public static String onlyNumbersSupportedErrorMessage = "ERROR - The calculator only accepts numbers.";
	public static String invalidOperationErrorMessage = "ERROR - Invalid operation requested.";
	public static String baseErrorMessage = "ERROR - Operation Failed!";
	public static String baseSuccessMessage = "SUCCESS - The result of the operation is : ";
}
