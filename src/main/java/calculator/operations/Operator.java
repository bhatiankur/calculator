package calculator.operations;

import java.math.BigDecimal;

/**
 * The generic definition of an operator which performs an operation, using the supplied operands
 * @author Ankur Bhatia
 *
 */
public interface Operator {
	
	public BigDecimal perform(String oper1, String oper2, String operation);

}
