package calculator.operations;

import java.math.BigDecimal;
import java.math.RoundingMode;

import calculator.utilities.Constants;

/**
 * A simple operator implementation which performs addition, subtraction, division & multiplication.
 * @author Ankur Bhatia
 *
 */
public class SimpleOperatorImpl implements Operator{

	@Override
	public BigDecimal perform(String oper1, String oper2, String operation) {
		// TODO Auto-generated method stub
		
		BigDecimal result = null;
		BigDecimal bigOper1 = new BigDecimal(oper1);
		BigDecimal bigOper2 = new BigDecimal(oper2);
		
		String action = Constants.supportedOperationsMap.get(operation); 
		
		if(action.equalsIgnoreCase(Constants.addAction))
			result = bigOper1.add(bigOper2);		
		else if(action.equalsIgnoreCase(Constants.subtractAction))
			result = bigOper1.subtract(bigOper2);				
		else if(action.equalsIgnoreCase(Constants.multiplyAction))
			result = bigOper1.multiply(bigOper2);		
		else if(action.equalsIgnoreCase(Constants.divideAction))
			result = bigOper1.divide(bigOper2, 2, RoundingMode.HALF_UP);
		else
			System.out.println(Constants.invalidOperationErrorMessage);
		
		
		
		return result;
		
	}

}
