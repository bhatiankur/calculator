package calculator.filtering;

import java.util.Arrays;
import java.util.List;

import calculator.utilities.Constants;

public class OperationSupportedCheckerImpl implements PreprocessingFilter {

	@Override
	public boolean evaluate(String[] args) {
		List<String> supportedList = Arrays.asList(Constants.supportedOperations);
		if(supportedList.contains(args[1]))
			return true;
		else
			System.out.println( Constants.supportedOperationsErrorMessage + supportedList.toString());
			
		return false;
	}

}
