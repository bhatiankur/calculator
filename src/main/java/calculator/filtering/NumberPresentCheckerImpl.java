package calculator.filtering;

import calculator.utilities.Constants;

public class NumberPresentCheckerImpl implements PreprocessingFilter {

	@Override
	public boolean evaluate(String[] args) {

		String operands [] = new String []{args[0], args[2]};	
		boolean isNumber = false;
		
		for(String operand : operands){	
			
			if (!(operand.matches(Constants.desiredOperandExpression)))
			{
				isNumber = false;
				System.out.println( Constants.onlyNumbersSupportedErrorMessage);
				return isNumber;
			
			}else
				isNumber = true;
		}
		
		return isNumber;
	}

}
