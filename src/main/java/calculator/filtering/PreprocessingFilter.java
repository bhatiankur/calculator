package calculator.filtering;

/**
 * The general definition of a pre processing filter which evaluates the input arguments
 * 
 * @author Ankur Bhatia
 *
 */
public interface PreprocessingFilter {
	
	public boolean evaluate(String args []);

}
