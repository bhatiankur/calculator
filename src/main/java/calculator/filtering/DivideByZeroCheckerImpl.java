package calculator.filtering;

import java.math.BigDecimal;

import calculator.utilities.Constants;

public class DivideByZeroCheckerImpl implements PreprocessingFilter {

	@Override
	public boolean evaluate(String[] args) {
		// TODO Auto-generated method stub
		boolean isDivideByZero = false;
		
		if(args[1].trim().equalsIgnoreCase(Constants.divideSymbol)){
		
			BigDecimal divisor = new BigDecimal(args[2]);
			
			if(divisor.signum() == 0){				
				System.out.println("ERROR - Division by Zero is not allowed.");
				isDivideByZero = true;
				return !isDivideByZero;
			}else{
				isDivideByZero = false;
			}
			
		}		
		return !isDivideByZero;
	}

}
