package calculator.filtering;
/**
 * This class is a placeholder for the future requirements,e.g. when the operands are of type sin(a), cos(b) etc.
 * Before the Operator get called the operand can be converted to a number and the supplied input arguments can be replaced 
 * by converted values. 
 * 
 * @author Ankur Bhatia
 *
 */
public class OperandConversionNeededCheckImpl implements PreprocessingFilter {

	@Override
	public boolean evaluate(String[] args) {
		// TODO Auto-generated method stub
		return false;
	}

}
