package calculator.filtering;

import calculator.utilities.Constants;

public class ArgumentCountCheckerImpl implements PreprocessingFilter {

	@Override
	public boolean evaluate(String[] args) {
		
		if(args.length == 3)			
			return true;
		else
			System.out.println(Constants.countErrorMessage);

		return false;
	}

}
