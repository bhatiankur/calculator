package calculator.filtering;

import calculator.utilities.Constants;

public class NegativeNumberCheckerImpl implements PreprocessingFilter {

	@Override
	public boolean evaluate(String[] args) {

		String operands [] = new String [] {args[0], args[2]};	
		boolean isNotNegative = false;
		
		for(String operand : operands){
			if(operand.trim().startsWith("-")){
				isNotNegative = false;
				System.out.println( Constants.negativeNumberNotSupportedErrorMessage);
				return isNotNegative;						
			}
			else
				isNotNegative = true;
		}
		
		
		return isNotNegative;
	}

}
